//
//  FavouritesViewController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-24.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import FirebaseDatabase
import YelpAPI

class SavesController: UITableViewController{
    
    fileprivate var ref: FIRDatabaseReference!
    
    fileprivate var data: [String] = []
    
    fileprivate var recentPostData: [String] = []
    fileprivate var recentDateData: [String] = []
    
    fileprivate var isLoading = false
    
    fileprivate var offset = 0
    
}

//MARK: Controller Methods
extension SavesController{
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        loadClasses()
        
    }
    
}

//MARK: Setup
extension SavesController{
    
    fileprivate func setup(){
        ref = FIRDatabase.database().reference()
        loadNibs()
        
    }
    
    fileprivate func loadNibs(){
        tableView.register(nibName: "SavesCell")
        
    }
    
    fileprivate func loadClasses(){
        
        data = []
        recentPostData = []
        recentDateData = []
        
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Saves").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            guard value != nil else {
                self.tableView.reloadData()
                return
            }
            
            for name in value!{
                
                self.data.append(name.key as! String)
                
            }
            
            self.sortDataAlphabetically()
            
            self.loadClasses2()
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
    fileprivate func loadClasses2(){
        
        let total = data.count
        var count = 0
        
        for name in data{
            
            self.ref.child("RecentPosts").child(name).observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                
                let time = value?.value(forKey: "Time")
                self.recentDateData.append(time as! String)
                self.recentPostData.append(value?.value(forKey: "Text") as! String)
                
                count += 1
                
                if count == total{
                    self.tableView.reloadData()
                    
                }
                
            }) { (error) in
                print(error.localizedDescription)
                
            }
            
        }
        
    }
    
}

//MARK: TableView Methods
extension SavesController{
    
    override internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard indexPath.row < data.count else {
            return
            
        }
        
        UserDefaults.standard.set(data[indexPath.row], forKey: "Class")
        
        let postsController = PostsController.storyboardInstance() as! PostsController
        self.navigationController?.pushViewController(postsController, animated: true)
        
    }
    
    override internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue(identifier: "SavesCell") as! SavesCell
        cell.setName(name: data[indexPath.row])
        cell.setRecent(date: recentDateData[indexPath.row], post: recentPostData[indexPath.row])
        return cell
        
    }
    
    override internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
        
    }
    
}

//MARK: Sorts
extension SavesController{
    
    fileprivate func sortDataAlphabetically(){
        
        let sortedArray = data.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        data = sortedArray
    }
    
}





