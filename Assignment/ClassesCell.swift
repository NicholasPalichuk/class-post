//
//  RestaurantsCell.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-25.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class ClassesCell: UITableViewCell {

    var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    @IBOutlet var restaurantName: UILabel!
    @IBOutlet var recentPostDateL: UILabel!
    @IBOutlet var recentPostL: UILabel!
    @IBOutlet var saveB: UIButton!
    
}

extension ClassesCell{
    
    public func setRecent(date: String, post: String){
        
        recentPostDateL.text = date
        recentPostL.text = post
        
    }
    
    public func setName(name: String){
        restaurantName.text = name
        selectionStyle = .none
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Saves").child(name).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if !snapshot.exists() {
                
                self.saveB.isHidden = false
                
            }

            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
    @IBAction func saveClass(){
        
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Saves").child(restaurantName.text!).setValue(true)
        saveB.isHidden = true
        
    }
    
}
