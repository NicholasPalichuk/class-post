//
//  AppDelegate.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-24.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FacebookLogin
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FIRApp.configure()
        
        if FIRAuth.auth()?.currentUser != nil{
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBar")
            
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
        }
        
        return true
        
    }
    
}

