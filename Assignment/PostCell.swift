//
//  DetailsCell.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-28.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class PostCell: UITableViewCell {

    fileprivate var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    @IBOutlet fileprivate var textL: UILabel!
    @IBOutlet fileprivate var dateL: UILabel!
    @IBOutlet fileprivate var voteB: UIButton!
    
    fileprivate var didVote = true
    
    fileprivate var name = ""
    fileprivate var randomCode = ""
    fileprivate var voteI = 0
    
}

//MARK Set Values Method
extension PostCell{
    
    func setValues(name: String, randomCode: String, text: String, vote: Int, time: String){
        
        voteB.setTitle("\(vote)", for: .normal)
        textL.text = text
        dateL.text = time
    
        self.voteI = vote
        self.name = name
        self.randomCode = randomCode
        
        checkPressed()
        
    }
    
}

//MARK: Actions
extension PostCell{
    
    @IBAction fileprivate func vote(){
        
        if !didVote{
            
            voteI += 1
            voteB.setTitle("\(voteI)", for: .normal)
            
            voteB.isEnabled = false
            
            ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Voted").child(randomCode).setValue(true)
            ref.child("Classes").child(name).child(randomCode).child("Votes").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    
                    self.ref.child("Classes").child(self.name).child(self.randomCode).child("Votes").setValue((snapshot.value as! Int) + 1)
                    
                }
                
                
            }) { (error) in
                print(error.localizedDescription)
                
            }
            
            
        }
        
    }
    
}

//MARK: Check and Set if Voted Methods
extension PostCell{
    
    func disableVote(){
        
        voteB.isEnabled = false
        
    }
    
    func checkPressed(){
        
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Voted").child(randomCode).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if !snapshot.exists() {
                
                self.voteB.isEnabled = true
                self.didVote = false
                
            }
            
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
}
















