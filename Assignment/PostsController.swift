//
//  PostsController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-25.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import YelpAPI
import Firebase
import FirebaseDatabase

class PostsController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    fileprivate var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var newB: UIButton!
    @IBOutlet fileprivate var topB: UIButton!
    
    fileprivate var className: String!
    fileprivate var data: [NSDictionary.Iterator.Element] = []
    
}

//MARK: Controller Methods
extension PostsController{
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        loadData()
        
    }
    
}

//MARK: Setup
extension PostsController{
    
    fileprivate func setup(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        className = UserDefaults.standard.string(forKey: "Class")
        navigationItem.title = className
        
        loadNibs()
        
    }
    
    fileprivate func loadNibs(){
        
        self.tableView.register(nibName: "PostCell")
        
    }
    
    fileprivate func loadData(){
        
        data = []
        
        ref.child("Classes").child(className).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            guard value != nil else {
                return
                
            }
            
            for key in value!{
                
                if (key.value as! NSDictionary).value(forKey: "Votes") as! Int != -1{
                    
                    self.data.append(key)
                    
                }
                
            }
            
            self.sortDataByVote()
            self.tableView.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
}

//MARK: Actions
extension PostsController{
    
    @IBAction fileprivate func newAction(){
        
        sortDataByDate()
        newB.backgroundColor = UIColor.init(hexadecimal: 0x5D89E4)
        topB.backgroundColor = UIColor.init(hexadecimal: 0x699AFF)
        tableView.reloadData()
        
    }
    
    @IBAction fileprivate func topAction(){
        
        sortDataByVote()
        topB.backgroundColor = UIColor.init(hexadecimal: 0x5D89E4)
        newB.backgroundColor = UIColor.init(hexadecimal: 0x699AFF)
        tableView.reloadData()
        
    }
    
    @IBAction fileprivate func openText(){
        
        UserDefaults.standard.set(className, forKey: "name")
        
        let addTextController = AddTextController.storyboardInstance() as! AddTextController
        self.navigationController?.pushViewController(addTextController, animated: true)
        
    }
    
}

//MARK: TableView Methods
extension PostsController{
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue(identifier: "PostCell") as! PostCell
        
        let text = getTextData(indexPath: indexPath)
        let vote = getVoteData(indexPath: indexPath)
        let time = getDateData(indexPath: indexPath)
        let randomCode = getRandomCodeData(indexPath: indexPath)
        
        cell.selectionStyle = .none
        cell.disableVote()
        cell.setValues(name: className, randomCode:randomCode, text: text, vote: vote, time: time)
        
        return cell
        
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
        
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (getTextData(indexPath: indexPath).height(withConstrainedWidth: 320, font: UIFont(name: "Times", size: 14)!)+50)
        
    }
    
}

//MARK: Sorts
extension PostsController{
    
    fileprivate func sortDataByDate(){
        
        self.data.sort {
            
            item1, item2 in
            let vote1 = (item1.value as! NSDictionary).value(forKey: "Time") as! String
            let vote2 = (item2.value as! NSDictionary).value(forKey: "Time") as! String
            return vote1 > vote2
            
        }
        
    }
    
    fileprivate func sortDataByVote(){
        
        self.data.sort {
            
            item1, item2 in
            let vote1 = (item1.value as! NSDictionary).value(forKey: "Votes") as! Int
            let vote2 = (item2.value as! NSDictionary).value(forKey: "Votes") as! Int
            return vote1 > vote2
            
        }
        
    }
    
}

//MARK: Get Cell Data Methods
extension PostsController{
    
    fileprivate func getTextData(indexPath: IndexPath) -> String{
        
        return (data[indexPath.row].value as! NSDictionary).value(forKey: "Text") as! String
        
    }
    
    fileprivate func getVoteData(indexPath: IndexPath) -> Int{
        
        return (data[indexPath.row].value as! NSDictionary).value(forKey: "Votes") as! Int
        
    }
    
    fileprivate func getDateData(indexPath: IndexPath) -> String{
        
        return (data[indexPath.row].value as! NSDictionary).value(forKey: "Time") as! String
        
    }
    
    fileprivate func getRandomCodeData(indexPath: IndexPath) -> String{
        
        return (data[indexPath.row].key) as! String
        
    }
    
}







