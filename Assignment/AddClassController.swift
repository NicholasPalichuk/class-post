//
//  AddClassController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-28.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class AddClassController: UIViewController, UITextFieldDelegate{

    fileprivate var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    @IBOutlet fileprivate var nameText: UITextField!
    @IBOutlet fileprivate var numberText: UITextField!
    @IBOutlet fileprivate var scrollView: UIScrollView!
    
    fileprivate var textLength = 4

    fileprivate var isNameTextField = true
    
}

//MARK: Controller Methods
extension AddClassController{
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
}

//MARK: Setup
extension AddClassController{
    
    fileprivate func setup(){
        
        addKeyboardWillShowNotification()
        addKeyboardWillHideNotification()
        hideKeyboardWhenTappedAround()
        
    }
    
}

//MARK: TextField Delegate Methods
extension AddClassController{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        numberText.becomeFirstResponder()
        isNameTextField = false
        
        return false
        
    }
    
    @objc(textField:shouldChangeCharactersInRange:replacementString:) internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return checkLengthAndCharacters(textField: textField, range: range, string: string)
        
    }
    
}

//MARK: Actions
extension AddClassController{
    
    @IBAction fileprivate func nameTextSelected(){
        
        isNameTextField = true
        textLength = 4
        nameText.delegate = self
        
    }
    
    @IBAction fileprivate func numberTextSelected(){
        
        isNameTextField = false
        textLength = 3
        numberText.delegate = self
        
    }
    
    @IBAction fileprivate func submitAction(){
        
        let nameTextValue = nameText.text
        let numberTextValue = numberText.text
        
        if nameTextValue != "" && numberTextValue != ""{
            
            setPlaceholderText(nameTextValue: nameTextValue!, numberTextValue: numberTextValue!)
            
            _ = navigationController?.popViewController(animated: true)
            
        }else{
            
            present(controller: UIAlertController.custom(title: "Textfield empty", message: "Enter values into both textfields", handler: nil))
            
        }
        
    }
    
}

//MARK: Keyboard Notifications
extension AddClassController{
    
    override internal func keyboardWillShow(_ notification: Notification) {
        
        guard let height = notification.keyboardHeight else {
            return
            
        }
        
        scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: CGFloat(height))
        
    }
    
    override internal func keyboardWillHide(_ notification: Notification) {
        
        scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: 0)
        
    }
    
}

//MARK: Firebase SetValues
extension AddClassController{
    
    fileprivate func setPlaceholderText(nameTextValue: String, numberTextValue: String){
        
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Info").child("Posts").setValue(0)
        ref.child("Classes").child("\(nameText.text!) \(numberText.text!)").child("PLACEHOLDER").child("Text").setValue("")
        ref.child("Classes").child("\(nameText.text!) \(numberText.text!)").child("PLACEHOLDER").child("Time").setValue("No Posts Yet")
        ref.child("Classes").child("\(nameText.text!) \(numberText.text!)").child("PLACEHOLDER").child("Votes").setValue(-1)
        
        ref.child("RecentPosts").child("\(nameText.text!) \(numberText.text!)").child("Text").setValue("")
        ref.child("RecentPosts").child("\(nameText.text!) \(numberText.text!)").child("Time").setValue("No Posts Yet")
        
    }
    
}

//MARK: Check length
extension AddClassController{
    
    fileprivate func checkLengthAndCharacters(textField: UITextField, range: NSRange, string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        
        if (range.length + range.location > currentCharacterCount){
            return false
            
        }
        
        let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZ").inverted
        
        if string.rangeOfCharacter(from: set) != nil && isNameTextField{
            return false
            
        }
        
        let newLength = currentCharacterCount + string.characters.count - range.length
        
        return newLength <= textLength
        
    }
    
}











