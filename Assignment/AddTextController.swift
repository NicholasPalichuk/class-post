//
//  AddTextController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-29.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import FirebaseDatabase

class AddTextController: UIViewController, UITextViewDelegate{

    fileprivate var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    @IBOutlet fileprivate var textView: UITextView!
    @IBOutlet fileprivate var charLabel: UILabel!
    
    fileprivate var name = ""

}

//MARK: Controller Methods
extension AddTextController{
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
}

//MARK: Setup
extension AddTextController{
    
    fileprivate func setup(){
        
        hideKeyboardWhenTappedAround()
        
        name = UserDefaults.standard.string(forKey: "name")!
        
        textView.contentInset = UIEdgeInsetsMake(-60.0,0.0,0,0.0);
        textView.delegate = self
        
    }
    
}

//MARK: TextView Delegate Methods
extension AddTextController{
    
    @objc(textView:shouldChangeTextInRange:replacementText:) internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        charLabel.text = "\(300-numberOfChars)"
        return numberOfChars < 300;
        
    }
    
}

//MARK: Actions
extension AddTextController{
    
    @IBAction fileprivate func submitText(){
        
        if textView.text.characters.count >= 2{
            
            let textCode = ref.childByAutoId().key
            let dateCurrent = String.dateAsText(date: Date())
            let text = textView.text!
            
            setNewPosts(textCode: textCode, text: text, date: dateCurrent)
            
            _ = navigationController?.popViewController(animated: true)
            
        }
        
    }
    
}

//MARK: Firebase SetValues
extension AddTextController{
    
    fileprivate func setNewPosts(textCode: String, text: String, date: String){
        
        ref.child("Classes").child(name).child(textCode).child("Text").setValue(text)
        ref.child("Classes").child(name).child(textCode).child("Votes").setValue(0)
        ref.child("Classes").child(name).child(textCode).child("Time").setValue(date)
        
        ref.child("RecentPosts").child(name).child("Text").setValue(text)
        ref.child("RecentPosts").child(name).child("Time").setValue(date)
        
    }
    
}












