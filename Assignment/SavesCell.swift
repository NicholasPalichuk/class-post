//
//  FavouritesCell.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-25.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SavesCell: UITableViewCell {
    
    fileprivate var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    @IBOutlet fileprivate var restaurantName: UILabel!
    @IBOutlet fileprivate var recentPostDateL: UILabel!
    @IBOutlet fileprivate var recentPostL: UILabel!
    @IBOutlet fileprivate var deleteB: UIButton!
    
}

extension SavesCell{
    
    public func setRecent(date: String, post: String){
        
        recentPostDateL.text = date
        recentPostL.text = post
        
    }
    
    public func setName(name: String){
        
        restaurantName.text = name
        selectionStyle = .none
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Saves").child(name).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                
                self.deleteB.isHidden = false
                
            }
            
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
    @IBAction fileprivate func deleteClass(){
        
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Saves").child(restaurantName.text!).removeValue()
        deleteB.isHidden = true
        
    }
    
}
