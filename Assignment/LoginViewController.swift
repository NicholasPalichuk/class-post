//
//  LoginViewController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-24.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FacebookLogin
import FBSDKLoginKit

class LoginViewController: UIViewController{

    fileprivate var ref: FIRDatabaseReference!
    
    @IBOutlet fileprivate var emailField: UITextField!
    
}

//MARK: Controller Methods
extension LoginViewController{
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
}

//MARK: Setup
extension LoginViewController{
    
    fileprivate func setup(){
        ref = FIRDatabase.database().reference()
        hideKeyboardWhenTappedAround()
        
    }
    
}

//MARK: Actions
extension LoginViewController{
    
    @IBAction fileprivate func signInA(){
        
        FIRAuth.auth()?.signInAnonymously() { (user, error) in
            if error == nil{
                
                self.ref.child("Global").child("TotalPosts").observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    let value = snapshot.value as! Int
                    
                    if value > 0 {
                        
                        self.ref.child("Users").child(user!.uid).child("Info").child("Posts").setValue(1)
                        self.ref.child("Users").child(user!.uid).child("Info").child("Email").setValue(self.emailField.text)
                        self.ref.child("Global").child("TotalPosts").setValue(value-1)
                        
                    }else{
                        
                        self.ref.child("Users").child(user!.uid).child("Info").child("Posts").setValue(0)
                        self.ref.child("Users").child(user!.uid).child("Info").child("Email").setValue(self.emailField.text)
                        
                    }
                    
                    self.showStoryBoard(storyBoard: "TabBar")
                    
                }) { (error) in
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
        
    }
    
}

//MARK: Show storyboard method
extension LoginViewController{
    
    fileprivate func showStoryBoard(storyBoard: String){
        
        let storyBoardS = UIStoryboard(name:storyBoard, bundle: nil).instantiateInitialViewController()!
        storyBoardS.modalTransitionStyle = .coverVertical
        present(storyBoardS, animated: false, completion: nil)
        
    }
    
}
