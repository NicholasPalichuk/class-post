//
//  RestaurantsTableViewController.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-25.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import MapKit
import FBSDKLoginKit
import Firebase

class ClassesController: UITableViewController{
    
    fileprivate var ref: FIRDatabaseReference!
    
    fileprivate var data: [String] = []
    
    fileprivate var recentPostData: [String] = []
    fileprivate var recentDateData: [String] = []
    
    fileprivate var isLoading = false
    
    fileprivate var offset = 0
    
    fileprivate var posts = 0
    
    @IBOutlet fileprivate var newClassB: UIBarButtonItem!
    
}

//MARK: Controller Methods
extension ClassesController{
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        
        loadClasses()
        
        ref.child("Users").child((FIRAuth.auth()?.currentUser?.uid)!).child("Info").child("Posts").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists(){
                
                let value = snapshot.value as! Int
                
                self.posts = value
                
                self.newClassB.title = "+\(value)"
                
            }
            
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
}

//MARK: Setup
extension ClassesController{
    
    fileprivate func setup(){
        ref = FIRDatabase.database().reference()
        loadNibs()
        
    }
    
    fileprivate func loadNibs(){
        tableView.register(nibName: "ClassesCell")
        
    }
    
    fileprivate func loadClasses(){
        
        data = []
        recentPostData = []
        recentDateData = []
        
        ref.child("Classes").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            guard value != nil else {
                self.tableView.reloadData()
                return
            }
            
            for name in value!{
                
                self.data.append(name.key as! String)
                
            }
            
            self.sortDataAlphabetically()
            
            self.loadClasses2()
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
    }
    
    fileprivate func loadClasses2(){
        
        let total = self.data.count
        var count = 0
        
        for name in self.data{
            
            self.ref.child("RecentPosts").child(name).observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                
                let time = value?.value(forKey: "Time")
                self.recentDateData.append("\(time!)")
                self.recentPostData.append(value?.value(forKey: "Text") as! String)
                
                count += 1
                
                if count == total{
                    self.tableView.reloadData()
                    
                }
                
            }) { (error) in
                print(error.localizedDescription)
                
            }
            
        }
        
    }
    
}

//MARK: TableView Methods
extension ClassesController{
    
    override internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard indexPath.row < data.count else {
            return
            
        }
        
        UserDefaults.standard.set(data[indexPath.row], forKey: "Class")
        
        let postsController = PostsController.storyboardInstance() as! PostsController
        self.navigationController?.pushViewController(postsController, animated: true)
        
    }
    
    override internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue(identifier: "ClassesCell") as! ClassesCell
        cell.setName(name: data[indexPath.row])
        cell.setRecent(date: recentDateData[indexPath.row], post: recentPostData[indexPath.row])
        return cell
        
    }
    
    override internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
        
    }
    
}

//MARK: Actions
extension ClassesController{
    
    @IBAction fileprivate func addClass(){
    
        if posts >= 1{
            
            let addClassController = AddClassController.storyboardInstance() as! AddClassController
            self.navigationController?.pushViewController(addClassController, animated: true)
            
        }else{
            
            present(controller: UIAlertController.custom(title: "Out of posts", message: "You cant post anymore classes", handler: nil))
            
        }
        
    }
    
}

//MARK: Sorts
extension ClassesController{
    
    fileprivate func sortDataAlphabetically(){
        
        let sortedArray = data.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        data = sortedArray
    }
    
}



