//
//  UIView.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-26.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension UIView{
    
    @IBInspectable public var masksToBounds: Bool{
        get { return false }
        set {
            
            layer.masksToBounds = newValue
        }
    }
    
    @IBInspectable public var shadowColor: UIColor{
        get { return UIColor.black }
        set {
            
            layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable public var shadowOffsetX: CGFloat{
        get { return 0 }
        set {
            
            layer.shadowOffset.width = newValue
        }
    }
    
    @IBInspectable public var shadowOffsetY: CGFloat{
        get { return 0 }
        set {
            
            layer.shadowOffset.height = newValue
        }
    }
    
    
    @IBInspectable public var shadowOpacity: Float{
        get { return 0 }
        set {
            
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable public var shadowRadius: CGFloat{
        get { return 0 }
        set {
            
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat {
        get { return layer.cornerRadius}
        set {
            
            layer.cornerRadius  = newValue
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat {
        get { return layer.borderWidth}
        set {
            
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable public var borderColor: UIColor? {
        get { return nil }
        set {
            
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
}
