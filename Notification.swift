//
//  Notification.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-24.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension Notification{
    
    public var keyboardHeight: CGFloat?{
        return (userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height
    }
    
    public static func post(name:String, _ object: AnyObject?=nil){
        NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: object)
    }
}
