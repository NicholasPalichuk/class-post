//
//  String.swift
//  Assignment
//
//  Created by Nicholas Palichuk on 2017-03-28.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension String {
    
    public static func dateAsText(date: Date) -> String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let dateAsString = formatter.string(from: date)
        
        return dateAsString
        
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
        
    }
}
